# Nuxt + Firebase Minimal Starter

Nuxtページを、ローカルからFirebaseへホスト＆デプロイする。  
[こちら](https://nuxt-firebase-minimal.web.app)で公開中。

## 作成方法

### Firebase CLIのインストール

1. 以下のコマンドを実行する。

```bash
$ curl -sL https://firebase.tools | bash
```

### Firebaseプロジェクトの作成

1. [Firebase](https://console.firebase.google.com/)を開く。

2. 「プロジェクトを追加」を選択する。

「Googleアナリティクス」は無効にする。

プロジェクト名とプロジェクトIDが決まる。

3. プロジェクト名の横の「Sparkプラン」を選択して、  
「Blazeプラン」に変更する。

### Nuxtプロジェクトの作成〜デプロイ

1. 以下のコマンドで、Firebase CLIをログインする。

```bash
$ firebase login
```

2. Nuxtプロジェクトを作成し、ディレクトリ内に入る。

プロジェクト名は、上記のFirebaseのプロジェクト名と同じが望ましい。

```bash
$ npx nuxi init nuxt-firebase-minimal
$ cd nuxt-firebase-minimal
```

3. Firebaseプロジェクトを初期化する。

```bash
$ firebase init
```

  1. 「Hosting: Configure files for Firebase Hosting and (optionally) set up GitHub Action deploys」を選択。
  
  2. 「Use an existing project」を選択。
  
  3. 上記で作成したFirebaseプロジェクトを選択。
  
  4. 「public」を選択。
  
  5. 「Configure as a single-page app ...」は「n」を選択。
  
  6. 「Set up automatic builds ...」は「n」を選択。

4. 生成された public ディレクトリと、firebase.json を削除する。

5. nuxt.config.ts に、nitro.preset に firebase を指定する。

```ts
export default defineNuxtConfig({
  nitro: {
    preset: "firebase",
  },
})
```

6. パッケージをインストールする。

```bash
$ npm install --dev firebase-admin firebase-functions firebase-functions-test
```

7. ビルドする。

```bash
$ npm run build
```

firebase.json が生成される。

8. firebase.json の「&lt;your_project_id&gt;」を、FirebaseプロジェクトIDに書き換える。

9. デプロイする。

```bash
$ firebase deploy
```

  - Blazeプランに上げていない場合、エラーになる。

  - 「Could not find functions.yaml」のエラーが出た場合。

```bash
$ mkdir .output/server/node_modules/.bin && cp -r node_modules/.bin/firebase-functions .output/server/node_modules/.bin && cp -rfu node_modules/firebase-functions/ .output/server/node_modules
```

成功すれば、Firebaseコンソールとデプロイ先のURLが表示される。

## 参考

- [Firebase Hosting を利用してWebページを表示する手順](https://www.yururiwork.net/archives/1322)
- [FirebaseCLIリファレンス](https://firebase.google.com/docs/cli)
- [Nuxt 3 サイトを Firebase Hosting にデプロイする方法](https://zenn.dev/ytr0903/articles/b77927580cf03c)

